using Microsoft.VisualStudio.TestTools.UnitTesting;
using NYSS_WPF_ENCRYPTER;

namespace NYSS_WPF_ENCRYPTER_TESTS
{
    [TestClass]
    public class EncrypterTests
    {
        [TestMethod]
        public void CesarEncryptResultIsValid()
        {
            //arrange
            char testRu = '�';
            char testEn = 'j';
            char expectedRu = '�';
            char expectedEn = 'i';
            int step = 25;

            //act
            char resultRu = Encrypter.EncryptCesar(testRu,step,Encrypter.ruAlphabet);
            char resultEn = Encrypter.EncryptCesar(testEn, step, Encrypter.enAlphabet);
            //assert
            Assert.AreEqual(expectedRu, resultRu);
            Assert.AreEqual(expectedEn, resultEn);
        }
        [TestMethod]
        public void CesarDecryptResultIsValid()
        {
            //arrange
            char testRu = '�';
            char testEn = 'i';
            char expectedRu = '�';
            char expectedEn = 'j';
            int step = 25;

            //act
            char resultRu = Encrypter.DecryptCesar(testRu, step, Encrypter.ruAlphabet);
            char resultEn = Encrypter.DecryptCesar(testEn, step, Encrypter.enAlphabet);
            //assert
            Assert.AreEqual(expectedRu, resultRu);
            Assert.AreEqual(expectedEn, resultEn);
        }
        [TestMethod]
        public void VigenereEncryptResultIsValid()
        {
            //arrange
            string testRu = "�����2123dsd�����!";
            string testEn = "Con������gratulations!3�2";
            string keyRu = "��������";
            string keyEn = "scorpion";
            string expectedRu = "�����2123dsd�����!";
            string expectedEn = "Uqb������xgihhdchzdvg!3�2";

            //act
            string resultRu = Encrypter.Vigenere(testRu,keyRu,"�����������",Encrypter.ruAlphabet);
            string resultEn = Encrypter.Vigenere(testEn, keyEn, "�����������", Encrypter.enAlphabet);

            //Assert
            Assert.AreEqual(expectedRu,resultRu);
            Assert.AreEqual(expectedEn, resultEn);

        }
        [TestMethod]
        public void VigenereDecryptResultIsValid()
        {
            //arrange
            string expectedRu = "�����2123dsd�����!";
            string expectedEn = "Con������gratulations!3�2";
            string keyRu = "��������";
            string keyEn = "scorpion";
            string testRu = "�����2123dsd�����!";
            string testEn = "Uqb������xgihhdchzdvg!3�2";

            //act
            string resultRu = Encrypter.Vigenere(testRu, keyRu, "������������", Encrypter.ruAlphabet);
            string resultEn = Encrypter.Vigenere(testEn, keyEn, "������������", Encrypter.enAlphabet);

            //Assert
            Assert.AreEqual(expectedRu, resultRu);
            Assert.AreEqual(expectedEn, resultEn);
        }
    }
}
