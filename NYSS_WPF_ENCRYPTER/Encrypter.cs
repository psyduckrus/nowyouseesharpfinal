﻿using System;
using System.Collections.Generic;

namespace NYSS_WPF_ENCRYPTER
{
    public class Encrypter
    {
        public static List<char> ruAlphabet = new List<char>();
        public static List<char> enAlphabet = new List<char>();
        public delegate void MethodContainer();
        public event MethodContainer changes;
        public string Result { get; set; }
        static Encrypter()
        {
            
            for (int i = 1040; i < 1072; i++)
            {
                if (ruAlphabet.Count == 6) ruAlphabet.Add('Ё');
                ruAlphabet.Add(Convert.ToChar(i));
            }
            for (int i = 65; i < 91; i++)
            {
                enAlphabet.Add(Convert.ToChar(i));
            }
        }
        public Encrypter(string language, string path, string key, string action)
        {
            changes += Vigenere;
            if (language == "Русский")
            {
                Result = Vigenere(path, key, action, ruAlphabet);
            }
            else
            {
                Result = Vigenere(path, key, action, enAlphabet);
            }
        }

        private void Vigenere()
        {
            throw new NotImplementedException();
        }

        public static string Vigenere(string text, string key, string action, List<char> alphabet) 
        {
            if (key == "") return "Ключ не был введен!";
            for (int i = 0; i < key.Length; i++)
            {
                if (!alphabet.Contains(Char.ToUpper(key[i]))) return "Язык ключа должен совпадать с языком шифрования!";
            } 
            if (key.Length < text.Length)
            {
                string realKey = "";
                int counter = 0;
                for (int i = 0; i < text.Length; i++)
                {
                    if (!alphabet.Contains(Char.ToUpper(text[i])))
                    {
                        realKey += text[i];
                        continue;
                    }
                    if (counter == key.Length) counter = 0;
                    realKey += key[counter];
                    counter++;
                }
                key = realKey;
            }
            char[] tmpArray = text.ToCharArray();
            int cesarStep;
            if (action == "Зашифровать")
            {
                for (int i = 0; i < text.Length; i++)
                {
                    if (!alphabet.Contains(Char.ToUpper(key[i])))
                    {
                        continue;
                    }
                    cesarStep = alphabet.IndexOf(Char.ToUpper(key[i]));
                    tmpArray[i] = EncryptCesar(tmpArray[i], cesarStep, alphabet);
                }
            }
            else
            {
                for (int i = 0; i < text.Length; i++)
                {
                    if (!alphabet.Contains(Char.ToUpper(key[i])))
                    {
                        continue;
                    }
                    cesarStep = alphabet.IndexOf(Char.ToUpper(key[i]));
                    tmpArray[i] = DecryptCesar(tmpArray[i], cesarStep, alphabet);
                }
            }
            return new string(tmpArray);
        }
        public static char DecryptCesar(char symbol, int step, List<char> alphabet)
        {
            char newSymbol = Char.ToUpper(symbol);

            if (alphabet.Contains(newSymbol))
            {
                if (alphabet.IndexOf(newSymbol) - step < 0)
                {
                    newSymbol = alphabet[alphabet.Count - ((step - alphabet.IndexOf(newSymbol)))];
                }
                else
                {
                    newSymbol = alphabet[(alphabet.IndexOf(newSymbol) - step)];
                }
                if (Char.IsUpper(symbol)) return Char.ToUpper(newSymbol);
                if (Char.IsLower(symbol)) return Char.ToLower(newSymbol);
            }
            return newSymbol;
        }
        public static char EncryptCesar(char symbol, int step, List<char> alphabet)
        {
            char newSymbol = Char.ToUpper(symbol);

            if (alphabet.Contains(newSymbol))
            {
                if (alphabet.IndexOf(newSymbol) + step + 1 > alphabet.Count)
                {
                    newSymbol = alphabet[((alphabet.IndexOf(newSymbol) + step) - alphabet.Count)];
                }
                else
                {
                    newSymbol = alphabet[(alphabet.IndexOf(newSymbol) + step)];
                }
                if (Char.IsUpper(symbol)) return Char.ToUpper(newSymbol);
                if (Char.IsLower(symbol)) return Char.ToLower(newSymbol);
            }
            return newSymbol;
        }
    }
}
