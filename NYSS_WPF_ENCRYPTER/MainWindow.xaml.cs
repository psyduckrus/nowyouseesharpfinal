﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;

namespace NYSS_WPF_ENCRYPTER
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

            InitializeComponent();
            cboLanguage.ItemsSource = languages;
            cboAction.ItemsSource = options;

        }

        public string Action { get { return (string)cboAction.SelectedItem; } }
        public string SelectedLanguage { get { return (string)cboLanguage.SelectedItem; } }
        public OpenFileDialog ofd = new OpenFileDialog() { Filter = "Text Documents|*.doc;*.docx;*.txt" };
        public SaveFileDialog sfd = new SaveFileDialog() { Filter = "TXT Format|*.txt|DOC Format|*.doc|DOCX Format|*.docx;" };
        public static readonly List<string> languages = new List<string> { "Русский", "Английский" };
        public static readonly List<string> options = new List<string> { "Расшифровать", "Зашифровать" };
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            tbInput.Text = "";
            ofd.ShowDialog();
            string path = ofd.FileName;
            try
            {
                if (path != "")
                {
                    if (path.Split('.')[path.Split('.').Length - 1] == "txt")
                    {
                        tbInput.Text = File.ReadAllText(path, Encoding.GetEncoding(1251));
                    }
                    else
                    {
                        Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
                        Object fileName = path;
                        app.Documents.Open(ref fileName);
                        Microsoft.Office.Interop.Word.Document doc = app.ActiveDocument;
                        for (int i = 1; i < doc.Paragraphs.Count + 1; i++)
                        {
                            tbInput.Text += doc.Paragraphs[i].Range.Text;
                        }
                        app.Quit();
                    }
                }
                else MessageBox.Show("Файл не был выбран!");
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
            ofd.FileName = "";
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {

            if (tbOutput.Text == "" || tbOutput.Text == null) MessageBox.Show("Нечего же сохранять!");
            else
            {
                sfd.ShowDialog();
                string path = sfd.FileName;
                try
                {
                    if (path != "")
                    {
                        if (path.Split('.')[path.Split('.').Length - 1] == "txt")
                        {
                            File.WriteAllText(sfd.FileName, tbOutput.Text, Encoding.GetEncoding(1251));
                        }
                        else
                        {
                            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
                            Object fileName = path;
                            app.Documents.Add();
                            Microsoft.Office.Interop.Word.Document doc = app.ActiveDocument;
                            doc.Content.Text = tbOutput.Text;
                            doc.SaveAs(ref fileName);
                            app.Quit();
                        }
                    }
                    else MessageBox.Show("Путь не был выбран!");
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
            }
            sfd.FileName = "";
        }

        private void btnAction_Click(object sender, RoutedEventArgs e)
        {

            tbOutput.Text = new Encrypter(SelectedLanguage, tbInput.Text, tbKey.Text, Action).Result;
        }
        private void cboLanguage_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            tbOutput.Text = new Encrypter(SelectedLanguage, tbInput.Text, tbKey.Text, Action).Result;
        }

        private void cboAction_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            tbOutput.Text = new Encrypter(SelectedLanguage, tbInput.Text, tbKey.Text, Action).Result;
        }

        private void tbKey_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            tbOutput.Text = new Encrypter(SelectedLanguage, tbInput.Text, tbKey.Text, Action).Result;
        }
        private void tbInputtChanged_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            tbOutput.Text = new Encrypter(SelectedLanguage, tbInput.Text, tbKey.Text, Action).Result;
        }
    }
}
